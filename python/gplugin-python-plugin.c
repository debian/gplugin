/*
 * Copyright (C) 2011-2014 Gary Kramlich <grim@reaperworld.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include <Python.h>

#include "gplugin-python-plugin.h"

#include <pygobject.h>

/******************************************************************************
 * Typedefs
 *****************************************************************************/
struct _GPluginPythonPlugin {
	GObject parent;

	PyObject *module;
	PyObject *query;
	PyObject *load;
	PyObject *unload;

	/* overrides */
	gchar *filename;
	GPluginLoader *loader;
	GPluginPluginInfo *info;
	GPluginPluginState state;
};

/******************************************************************************
 * Enums
 *****************************************************************************/
enum {
	PROP_ZERO,
	PROP_MODULE,
	PROP_LOAD_FUNC,
	PROP_UNLOAD_FUNC,
	N_PROPERTIES,
	/* overrides */
	PROP_FILENAME = N_PROPERTIES,
	PROP_LOADER,
	PROP_INFO,
	PROP_STATE
};
static GParamSpec *properties[N_PROPERTIES] = {NULL,};

/* I hate forward declarations... */
static void gplugin_python_plugin_iface_init(GPluginPluginInterface *iface);

G_DEFINE_DYNAMIC_TYPE_EXTENDED(
	GPluginPythonPlugin,
	gplugin_python_plugin,
	G_TYPE_OBJECT,
	0,
	G_IMPLEMENT_INTERFACE(GPLUGIN_TYPE_PLUGIN, gplugin_python_plugin_iface_init)
);

/******************************************************************************
 * GPluginPlugin Implementation
 *****************************************************************************/
static void
gplugin_python_plugin_iface_init(G_GNUC_UNUSED GPluginPluginInterface *iface) {
}

/******************************************************************************
 * Private Stuff
 *****************************************************************************/
static void
gplugin_python_plugin_set_module(GPluginPythonPlugin *plugin,
                                 PyObject *module)
{
	g_return_if_fail(GPLUGIN_IS_PLUGIN(plugin));
	g_return_if_fail(module);

	Py_XINCREF(module);
	Py_CLEAR(plugin->module);
	plugin->module = module;
}

static gpointer
gplugin_python_plugin_get_load_func(GPluginPythonPlugin *plugin) {
	g_return_val_if_fail(GPLUGIN_PYTHON_IS_PLUGIN(plugin), NULL);

	return plugin->load;
}

static void
gplugin_python_plugin_set_load_func(GPluginPythonPlugin *plugin,
                                     PyObject *func)
{
	g_return_if_fail(GPLUGIN_PYTHON_IS_PLUGIN(plugin));
	g_return_if_fail(func != NULL);

	Py_XINCREF(func);
	Py_CLEAR(plugin->load);
	plugin->load = func;
}

static gpointer
gplugin_python_plugin_get_unload_func(GPluginPythonPlugin *plugin) {
	g_return_val_if_fail(GPLUGIN_PYTHON_IS_PLUGIN(plugin), NULL);

	return plugin->unload;
}

static void
gplugin_python_plugin_set_unload_func(GPluginPythonPlugin *plugin,
                                     PyObject *func)
{
	g_return_if_fail(GPLUGIN_PYTHON_IS_PLUGIN(plugin));
	g_return_if_fail(func != NULL);

	Py_XINCREF(func);
	Py_CLEAR(plugin->unload);
	plugin->unload = func;
}

/******************************************************************************
 * Object Stuff
 *****************************************************************************/
static void
gplugin_python_plugin_get_property(GObject *obj, guint param_id, GValue *value,
                                   GParamSpec *pspec)
{
	GPluginPythonPlugin *plugin = GPLUGIN_PYTHON_PLUGIN(obj);

	switch(param_id) {
		case PROP_MODULE:
			g_value_set_pointer(value, plugin->module);
			break;
		case PROP_LOAD_FUNC:
			g_value_set_pointer(value,
			                    gplugin_python_plugin_get_load_func(plugin));
			break;
		case PROP_UNLOAD_FUNC:
			g_value_set_pointer(value,
			                    gplugin_python_plugin_get_unload_func(plugin));
			break;

		/* overrides */
		case PROP_FILENAME:
			g_value_set_string(value, plugin->filename);
			break;
		case PROP_LOADER:
			g_value_set_object(value, plugin->loader);
			break;
		case PROP_INFO:
			g_value_set_object(value, plugin->info);
			break;
		case PROP_STATE:
			g_value_set_enum(value, plugin->state);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, param_id, pspec);
			break;
	}
}

static void
gplugin_python_plugin_set_property(GObject *obj, guint param_id,
                                   const GValue *value, GParamSpec *pspec)
{
	GPluginPythonPlugin *plugin = GPLUGIN_PYTHON_PLUGIN(obj);

	switch(param_id) {
		case PROP_MODULE:
			gplugin_python_plugin_set_module(plugin,
			                                 g_value_get_pointer(value));
			break;
		case PROP_LOAD_FUNC:
			gplugin_python_plugin_set_load_func(plugin,
			                                    g_value_get_pointer(value));
			break;
		case PROP_UNLOAD_FUNC:
			gplugin_python_plugin_set_unload_func(plugin,
			                                      g_value_get_pointer(value));
			break;

		/* overrides */
		case PROP_FILENAME:
			plugin->filename = g_value_dup_string(value);
			break;
		case PROP_LOADER:
			plugin->loader = g_value_dup_object(value);
			break;
		case PROP_INFO:
			plugin->info = g_value_dup_object(value);
			break;
		case PROP_STATE:
			plugin->state = g_value_get_enum(value);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, param_id, pspec);
			break;
	}
}

static void
gplugin_python_plugin_finalize(GObject *obj) {
	GPluginPythonPlugin *plugin = GPLUGIN_PYTHON_PLUGIN(obj);

	Py_CLEAR(plugin->module);
	Py_CLEAR(plugin->load);
	Py_CLEAR(plugin->unload);

	g_clear_pointer(&plugin->filename, g_free);
	g_clear_object(&plugin->loader);
	g_clear_object(&plugin->info);

	G_OBJECT_CLASS(gplugin_python_plugin_parent_class)->finalize(obj);
}

static void
gplugin_python_plugin_init(G_GNUC_UNUSED GPluginPythonPlugin *plugin) {
}

static void
gplugin_python_plugin_class_finalize(G_GNUC_UNUSED GPluginPythonPluginClass *klass)
{
}

static void
gplugin_python_plugin_class_init(GPluginPythonPluginClass *klass) {
	GObjectClass *obj_class = G_OBJECT_CLASS(klass);

	obj_class->get_property = gplugin_python_plugin_get_property;
	obj_class->set_property = gplugin_python_plugin_set_property;
	obj_class->finalize = gplugin_python_plugin_finalize;

	properties[PROP_MODULE] = g_param_spec_pointer(
		"module", "module",
		"The python module object",
		G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY
	);

	properties[PROP_LOAD_FUNC] = g_param_spec_pointer(
		"load-func", "load-func",
		"The python load function",
		G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY
	);

	properties[PROP_UNLOAD_FUNC] = g_param_spec_pointer(
		"unload-func", "unload-func",
		"The python unload function",
		G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY
	);

	g_object_class_install_properties(obj_class, N_PROPERTIES, properties);

	/* add our overrides */
	g_object_class_override_property(obj_class, PROP_FILENAME, "filename");
	g_object_class_override_property(obj_class, PROP_LOADER, "loader");
	g_object_class_override_property(obj_class, PROP_INFO, "info");
	g_object_class_override_property(obj_class, PROP_STATE, "state");
}

/******************************************************************************
 * API
 *****************************************************************************/
void
gplugin_python_plugin_register(GPluginNativePlugin *native) {
	gplugin_python_plugin_register_type(G_TYPE_MODULE(native));
}
