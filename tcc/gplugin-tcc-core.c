/*
 * Copyright (C) 2011-2013 Gary Kramlich <grim@reaperworld.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gplugin.h>
#include <gplugin-native.h>

#include <glib/gi18n-lib.h>

#include "gplugin-tcc-loader.h"
#include "gplugin-tcc-plugin.h"


G_MODULE_EXPORT GPluginPluginInfo *
gplugin_query(G_GNUC_UNUSED GError **error) {
	const gchar * const authors[] = {
		"Eion Robb <eion@robbmob.com>",
		NULL
	};

	return gplugin_plugin_info_new(
		"gplugin/tcc-loader",
		GPLUGIN_NATIVE_PLUGIN_ABI_VERSION,
		"internal", TRUE,
		"load-on-query", TRUE,
		"name", "C source plugin loader",
		"version", GPLUGIN_VERSION,
		"license-id", "LGPL2",
		"summary", "A plugin that can load C source plugins",
		"description", "This plugin allows the loading of plugins written in C.",
		"authors", authors,
		"website", GPLUGIN_WEBSITE,
		"category", "loaders",
		NULL
	);
}

G_MODULE_EXPORT gboolean
gplugin_load(GPluginNativePlugin *plugin,
             G_GNUC_UNUSED GError **error)
{
	gplugin_tcc_loader_register(plugin);
	gplugin_tcc_plugin_register(plugin);

	gplugin_manager_register_loader(GPLUGIN_TCC_TYPE_LOADER);

	return TRUE;
}

G_MODULE_EXPORT gboolean
gplugin_unload(G_GNUC_UNUSED GPluginNativePlugin *plugin,
               GError **error)
{
	g_set_error_literal(
		error,
		GPLUGIN_DOMAIN,
		0,
		_("The TCC loader can not be unloaded")
	);

	return FALSE;
}

