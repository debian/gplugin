# GPlugin

GPlugin is a GObject based library that implements a reusable plugin system.
It supports loading plugins in multiple other languages via loaders.  It relies
heavily on [GObjectIntrospection](https://gi.readthedocs.io/) to expose its API
to the other languages.

It has a simple API which makes it very easy to use in your application.
For more information on using GPlugin in your application, please see the
[embedding](Embedding) page.

## Build Status

Default: [![Default Build Status](https://bamboo.pidgin.im/plugins/servlet/wittified/build-status/GPLUG-GPLUGIN)](https://bamboo.pidgin.im/browse/GPLUG-GPLUGIN)

Develop: [![Develop Build Status](https://bamboo.pidgin.im/plugins/servlet/wittified/build-status/GPLUG-GPLUGIN0)](https://bamboo.pidgin.im/browse/GPLUG-GPLUGIN0/)

## History

GPlugin has a bit of history, you can read more about it in [HISTORY.md](HISTORY.md)

## Language Support

GPlugin currently supports plugins written in C/C++, Lua, Python, and Vala.

## API Reference

The API reference for the `default` branch can be found at
[docs.pidgin.im/gplugin/default](https://docs.pidgin.im/gplugin/default).

The in-development API reference for the `develop` branch can be found at
[docs.pidgin.im/gplugin/develop](https://docs.pidgin.im/gplugin/develop).
