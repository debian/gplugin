/*
 * Copyright (C) 2011-2014 Gary Kramlich <grim@reaperworld.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include <gplugin/gplugin-loader.h>

#include <gplugin/gplugin-core.h>

/**
 * GPLUGIN_TYPE_LOADER:
 *
 * The standard _get_type macro for #GPluginLoader.
 */

/**
 * SECTION:gplugin-loader
 * @Title: Plugin Loader Interface
 * @Short_description: interface for loading plugins
 *
 * A PluginLoader has to implement the interface described here for GPlugin to
 * be able to use it to load plugins.
 */

/**
 * GPluginLoader:
 *
 * An abstract data type that should not be accessed directly.
 */

/**
 * GPluginLoaderClass:
 * @supported_extensions: The supported_extensions vfunc returns a #GList of
 *                        file extensions that this loader supports without the
 *                        leading dot. For example: 'so', 'dll', 'py', etc.
 * @query: The query vfunc is called when the plugin manager needs to query a
 *         plugin that has a file extension from @supported_extensions.
 * @load: The load vfunc is called when the plugin manager wants to load a
 *        plugin that was previously queried by this loader.
 * @unload: The unload vfunc is called when the plugin manager wants to unload
 *          a previously loaded plugin from this loader.
 *
 * #GPluginLoader class defines the behavior for loading plugins.
 */

/******************************************************************************
 * GObject Stuff
 *****************************************************************************/
G_DEFINE_ABSTRACT_TYPE(GPluginLoader, gplugin_loader, G_TYPE_OBJECT);

static void
gplugin_loader_init(G_GNUC_UNUSED GPluginLoader *loader) {
}

static void
gplugin_loader_class_init(G_GNUC_UNUSED GPluginLoaderClass *klass) {
}

/******************************************************************************
 * API
 *****************************************************************************/

/**
 * gplugin_loader_query_plugin:
 * @loader: #GPluginLoader instance performing the query
 * @filename: filename to query
 * @error: return location for a GError, or NULL
 *
 * This function is called by the plugin manager to ask a loader to query the
 * given file and determine if it's a usable plugin.
 *
 * Return value: (transfer full): A #GPluginPlugin instance or NULL on failure
 */
GPluginPlugin *
gplugin_loader_query_plugin(GPluginLoader *loader,
                            const gchar *filename, GError **error)
{
	GPluginLoaderClass *klass = NULL;

	g_return_val_if_fail(loader != NULL, NULL);
	g_return_val_if_fail(GPLUGIN_IS_LOADER(loader), NULL);
	g_return_val_if_fail(filename, NULL);
	g_return_val_if_fail(error != NULL, NULL);

	klass = GPLUGIN_LOADER_GET_CLASS(loader);

	if(klass && klass->query)
		return klass->query(loader, filename, error);

	return NULL;
}

/**
 * gplugin_loader_load_plugin:
 * @loader: #GPluginLoader instance performing the load
 * @plugin: #GPluginPlugin instance to load
 * @error: return location for a GError, or NULL
 *
 * This function is called by the plugin manager to ask a loader to load the
 * given plugin.
 *
 * Return value: TRUE if @plugin was loaded successfully, FALSE otherwise
 */
gboolean
gplugin_loader_load_plugin(GPluginLoader *loader,
                           GPluginPlugin *plugin, GError **error)
{
	GPluginLoaderClass *klass = NULL;
	gboolean ret = FALSE;

	g_return_val_if_fail(loader != NULL, FALSE);
	g_return_val_if_fail(GPLUGIN_IS_LOADER(loader), FALSE);
	g_return_val_if_fail(GPLUGIN_IS_PLUGIN(plugin), FALSE);

	klass = GPLUGIN_LOADER_GET_CLASS(loader);

	if(klass && klass->load)
		ret = klass->load(loader, plugin, error);

	if (!ret && error && *error == NULL) {
		g_set_error(error, GPLUGIN_DOMAIN, 0,
		            "Failed to load plugin : unknown");
	}

	return ret;
}

/**
 * gplugin_loader_unload_plugin:
 * @loader: #GPluginLoader instance performing the unload
 * @plugin: #GPluginPlugin instance to unload
 * @error: return location for a GError, or NULL
 *
 * This function is called by the plugin manager to ask a loader to unload the
 * given plugin.
 *
 * Return value: TRUE if @plugin was unloaded successfully, FALSE otherwise
 */
gboolean
gplugin_loader_unload_plugin(GPluginLoader *loader,
                             GPluginPlugin *plugin, GError **error)
{
	GPluginLoaderClass *klass = NULL;
	gboolean ret = FALSE;

	g_return_val_if_fail(loader != NULL, FALSE);
	g_return_val_if_fail(GPLUGIN_IS_LOADER(loader), FALSE);
	g_return_val_if_fail(GPLUGIN_IS_PLUGIN(plugin), FALSE);

	klass = GPLUGIN_LOADER_GET_CLASS(loader);

	if(klass && klass->unload)
		ret = klass->unload(loader, plugin, error);

	if (!ret && error && *error == NULL) {
		g_set_error(error, GPLUGIN_DOMAIN, 0,
		            "Failed to unload plugin : unknown");
	}

	return ret;
}

/**
 * gplugin_loader_class_get_supported_extensions:
 * @klass: #GPluginLoader instance
 *
 * Returns a #GSList of string for which extensions the loader supports.
 *
 * Return value: (element-type utf8) (transfer container): A #GSList of
 *               extensions that the loader supports.
 */
GSList *
gplugin_loader_class_get_supported_extensions(GPluginLoaderClass *klass) {
	g_return_val_if_fail(GPLUGIN_IS_LOADER_CLASS(klass), NULL);

	if(klass->supported_extensions)
		return klass->supported_extensions(klass);

	return NULL;
}

