/*
 * Copyright (C) 2013 Ankit Vani <a@nevitus.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include <gplugin.h>
#include <gplugin-native.h>

#include "dynamic-test.h"

G_DEFINE_DYNAMIC_TYPE(DynamicTest, dynamic_test, G_TYPE_OBJECT);

static void
dynamic_test_init(G_GNUC_UNUSED DynamicTest *inst) {
	g_message("instance created");
}

static void
dynamic_test_class_finalize(G_GNUC_UNUSED DynamicTestClass *klass) {
}

static void
dynamic_test_class_init(G_GNUC_UNUSED DynamicTestClass *klass) {
	g_message("class created");
}

G_MODULE_EXPORT GPluginPluginInfo *
gplugin_query(G_GNUC_UNUSED GError **error) {
	return gplugin_plugin_info_new(
		"gplugin/dynamic-type-provider",
		GPLUGIN_NATIVE_PLUGIN_ABI_VERSION,
		NULL
	);
}

G_MODULE_EXPORT gboolean
gplugin_load(GPluginNativePlugin *plugin, G_GNUC_UNUSED GError **error) {
	dynamic_test_register_type(G_TYPE_MODULE(plugin));

	return TRUE;
}

G_MODULE_EXPORT gboolean
gplugin_unload(G_GNUC_UNUSED GPluginNativePlugin *plugin,
               G_GNUC_UNUSED GError **error)
{
	return TRUE;
}

