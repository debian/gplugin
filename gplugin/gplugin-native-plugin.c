/*
 * Copyright (C) 2011-2014 Gary Kramlich <grim@reaperworld.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include <gplugin/gplugin-native-plugin.h>

#include <gplugin/gplugin-native-private.h>

#include <gplugin/gplugin-loader.h>
#include <gplugin/gplugin-manager.h>

#include <gplugin/gplugin-core.h>

#include <glib/gi18n.h>

/**
 * SECTION:gplugin-native-plugin
 * @Title: Native Plugin API
 * @Short_description: API for native plugins
 *
 * API for use by native plugins.  That is plugins written in a compiled
 * language.
 */

/**
 * GPLUGIN_TYPE_NATIVE_PLUGIN:
 *
 * The standard _get_type macro for #GPluginNativePlugin.
 */

/**
 * GPluginNativePlugin:
 *
 * An instance of a loaded native plugin.  A native plugin is a plugin that was
 * compiled to machine native code, typically these are written in C/C++.
 */

/******************************************************************************
 * Structs
 *****************************************************************************/
struct _GPluginNativePlugin {
	GTypeModule parent;

	GModule *module;

	gpointer load_func;
	gpointer unload_func;

	gchar *filename;
	GPluginLoader *loader;
	GPluginPluginInfo *info;
	GPluginPluginState state;
};

/******************************************************************************
 * Enums
 *****************************************************************************/
enum {
	PROP_ZERO,
	PROP_MODULE,
	PROP_LOAD_FUNC,
	PROP_UNLOAD_FUNC,
	N_PROPERTIES,
	/* overrides */
	PROP_FILENAME = N_PROPERTIES,
	PROP_LOADER,
	PROP_INFO,
	PROP_STATE
};
static GParamSpec *properties[N_PROPERTIES] = {NULL,};

/******************************************************************************
 * GPluginPlugin Implementation
 *****************************************************************************/
static void
gplugin_native_plugin_iface_init(G_GNUC_UNUSED GPluginPluginInterface *iface) {
	/* we just override properites from GPluginPlugin */
}

/******************************************************************************
 * GTypeModule Implementation
 *****************************************************************************/
static gboolean
gplugin_native_plugin_load(G_GNUC_UNUSED GTypeModule *module) {
	return TRUE;
}

static void
gplugin_native_plugin_unload(G_GNUC_UNUSED GTypeModule *module) {
}

/******************************************************************************
 * GObject Implementation
 *****************************************************************************/
G_DEFINE_TYPE_WITH_CODE(
	GPluginNativePlugin,
	gplugin_native_plugin,
	G_TYPE_TYPE_MODULE,
	G_IMPLEMENT_INTERFACE(GPLUGIN_TYPE_PLUGIN, gplugin_native_plugin_iface_init)
);

static void
gplugin_native_plugin_get_property(GObject *obj, guint param_id, GValue *value,
                                   GParamSpec *pspec)
{
	GPluginNativePlugin *plugin = GPLUGIN_NATIVE_PLUGIN(obj);

	switch(param_id) {
		case PROP_MODULE:
			g_value_set_pointer(value,
			                    gplugin_native_plugin_get_module(plugin));
			break;
		case PROP_LOAD_FUNC:
			g_value_set_pointer(value, plugin->load_func);
			break;
		case PROP_UNLOAD_FUNC:
			g_value_set_pointer(value, plugin->unload_func);
			break;

		/* overrides */
		case PROP_FILENAME:
			g_value_set_string(value, plugin->filename);
			break;
		case PROP_LOADER:
			g_value_set_object(value, plugin->loader);
			break;
		case PROP_INFO:
			g_value_set_object(value, plugin->info);
			break;
		case PROP_STATE:
			g_value_set_enum(value, plugin->state);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, param_id, pspec);
			break;
	}
}

static void
gplugin_native_plugin_set_property(GObject *obj, guint param_id,
                                   const GValue *value, GParamSpec *pspec)
{
	GPluginNativePlugin *plugin = GPLUGIN_NATIVE_PLUGIN(obj);

	switch(param_id) {
		case PROP_MODULE:
			plugin->module = g_value_get_pointer(value);
			break;
		case PROP_LOAD_FUNC:
			plugin->load_func = g_value_get_pointer(value);
			break;
		case PROP_UNLOAD_FUNC:
			plugin->unload_func = g_value_get_pointer(value);
			break;

		/* overrides */
		case PROP_FILENAME:
			plugin->filename = g_value_dup_string(value);
			break;
		case PROP_LOADER:
			plugin->loader = g_value_dup_object(value);
			break;
		case PROP_INFO:
			plugin->info = g_value_dup_object(value);
			break;
		case PROP_STATE:
			plugin->state = g_value_get_enum(value);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, param_id, pspec);
			break;
	}
}

static void
gplugin_native_plugin_finalize(GObject *obj) {
	GPluginNativePlugin *plugin = GPLUGIN_NATIVE_PLUGIN(obj);

	g_clear_pointer(&plugin->filename, g_free);
	g_clear_object(&plugin->loader);
	g_clear_object(&plugin->info);

	G_OBJECT_CLASS(gplugin_native_plugin_parent_class)->finalize(obj);
}

static void
gplugin_native_plugin_init(G_GNUC_UNUSED GPluginNativePlugin *plugin) {
}

static void
gplugin_native_plugin_class_init(GPluginNativePluginClass *klass) {
	GObjectClass *obj_class = G_OBJECT_CLASS(klass);
	GTypeModuleClass *module_class = G_TYPE_MODULE_CLASS(klass);

	obj_class->finalize = gplugin_native_plugin_finalize;
	obj_class->get_property = gplugin_native_plugin_get_property;
	obj_class->set_property = gplugin_native_plugin_set_property;

	module_class->load = gplugin_native_plugin_load;
	module_class->unload = gplugin_native_plugin_unload;

	/**
	 * GPluginNativePlugin:module:
	 *
	 * The GModule instance for this plugin.
	 */
	properties[PROP_MODULE] = g_param_spec_pointer(
		"module", "module handle",
		"The GModule instance of the plugin",
		G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY
	);

	/**
	 * GPluginNativePlugin:load-func:
	 *
	 * A function pointer to the load method of the plugin.
	 */
	properties[PROP_LOAD_FUNC] = g_param_spec_pointer(
		"load-func", "load function pointer",
		"address pointer to load function",
		G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY
	);

	/**
	 * GPluginNativePlugin:unload-func:
	 *
	 * A function pointer to the unload method of the plugin.
	 */
	properties[PROP_UNLOAD_FUNC] = g_param_spec_pointer(
		"unload-func", "unload function pointer",
		"address pointer to the unload function",
		G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY
	);

	g_object_class_install_properties(obj_class, N_PROPERTIES, properties);

	/* add our overrides */
	g_object_class_override_property(obj_class, PROP_FILENAME, "filename");
	g_object_class_override_property(obj_class, PROP_LOADER, "loader");
	g_object_class_override_property(obj_class, PROP_INFO, "info");
	g_object_class_override_property(obj_class, PROP_STATE, "state");
}

/******************************************************************************
 * API
 *****************************************************************************/

/**
 * gplugin_native_plugin_get_module: (skip)
 * @plugin: #GPluginNativePlugin instance
 *
 * Returns the %GModule associated with this plugin.  This should really only
 * be used if you need to make your plugin resident.
 *
 * Returns: The %GModule associated with this plugin.
 */
GModule *
gplugin_native_plugin_get_module(GPluginNativePlugin *plugin) {
	g_return_val_if_fail(GPLUGIN_IS_NATIVE_PLUGIN(plugin), NULL);

	return plugin->module;
}

