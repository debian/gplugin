/*
 * Copyright (C) 2011-2014 Gary Kramlich <grim@reaperworld.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib/gi18n.h>
#include <glib-object.h>

#include <gplugin/gplugin-core.h>
#include <gplugin/gplugin-private.h>
#include <gplugin/gplugin-plugin.h>

/**
 * SECTION:gplugin-core
 * @Title: Core API
 * @Short_description: the core api
 *
 * This section contains the core api of gplugin, which includes #gplugin_init
 * and #gplugin_uninit.
 */

/******************************************************************************
 * API
 *****************************************************************************/

/**
 * GPLUGIN_DOMAIN: (skip)
 *
 * The #GError domain used internally by GPlugin
 */

/**
 * GPLUGIN_GLOBAL_HEADER_INSIDE: (skip)
 *
 * This define is used to determine if we're inside the gplugin global header
 * file or not.
 */

/**
 * gplugin_init:
 *
 * Initializes the GPlugin library.
 *
 * This function *MUST* be called before interacting with any other GPlugin
 * API. The one exception is gplugin_get_option_group(). Parsing options with
 * the GOptionGroup from gplugin_get_option_group() internally calls
 * gplugin_init().
 */
void
gplugin_init(void) {
	bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");

	gplugin_manager_private_init();
}

/**
 * gplugin_uninit:
 *
 * Uninitializes the GPlugin library
 */
void
gplugin_uninit(void) {
	gplugin_manager_private_uninit();
}

