/*
 * Copyright (C) 2011-2014 Gary Kramlich <grim@reaperworld.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#if !defined(GPLUGIN_GLOBAL_HEADER_INSIDE) && !defined(GPLUGIN_COMPILATION)
#error "only <gplugin.h> or <gplugin-native.h> may be included directly"
#endif

#ifndef GPLUGIN_NATIVE_PLUGIN_H
#define GPLUGIN_NATIVE_PLUGIN_H

#include <gmodule.h>

#include <gplugin/gplugin-plugin.h>

G_BEGIN_DECLS

#define GPLUGIN_TYPE_NATIVE_PLUGIN (gplugin_native_plugin_get_type())
G_DECLARE_FINAL_TYPE(GPluginNativePlugin, gplugin_native_plugin, GPLUGIN, NATIVE_PLUGIN, GTypeModule)

GModule *gplugin_native_plugin_get_module(GPluginNativePlugin *plugin);

G_END_DECLS

#endif /* GPLUGIN_NATIVE_PLUGIN_H */

