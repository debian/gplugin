/*
 * Copyright (C) 2011-2014 Gary Kramlich <grim@reaperworld.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "gplugin-perl-loader.h"

#include <EXTERN.h>
#include <perl.h>

/* perl define's _() to something completely different that we don't use.  So
 * we undef it so that we can use it for gettext.
 */
#undef _
#include <glib/gi18n.h>

struct _GPluginPerlLoader {
	GPluginLoader parent;
};

G_DEFINE_DYNAMIC_TYPE(GPluginPerlLoader, gplugin_perl_loader, GPLUGIN_TYPE_LOADER);

/* I can't believe I have to use this variable name... */
static PerlInterpreter *my_perl = NULL;

/******************************************************************************
 * GPluginLoaderInterface API
 *****************************************************************************/
static GSList *
gplugin_perl_loader_class_supported_extensions(const GPluginLoaderClass *klass) {
	return g_slist_append(NULL, "pl");
}

static GPluginPlugin *
gplugin_perl_loader_query(GPluginLoader *loader,
                                   const gchar *filename,
                                   GError **error)
{
	const gchar *args[] = { "", filename };
	gchar **argv = (gchar **)args;
	gint argc = 2;

	perl_parse(my_perl, NULL, argc, argv, NULL);

	call_argv("gplugin_plugin_query", G_DISCARD | G_NOARGS, argv);

	return NULL;
}

static gboolean
gplugin_perl_loader_load(GPluginLoader *loader,
                                  GPluginPlugin *plugin,
                                  GError **error)
{
	return FALSE;
}

static gboolean
gplugin_perl_loader_unload(GPluginLoader *loader,
                                    GPluginPlugin *plugin,
                                    GError **error)
{
	return FALSE;
}

/******************************************************************************
 * Perl Stuff
 *****************************************************************************/
static void
gplugin_perl_loader_init_perl(void) {
	gchar *args[] = { "", };
	gchar **argv = (gchar **)args;
	gint argc = 1;

	PERL_SYS_INIT(&argc, &argv);

	my_perl = perl_alloc();
	PERL_SET_CONTEXT(my_perl);
	PL_perl_destruct_level = 1;
	perl_construct(my_perl);
}

static void
gplugin_perl_loader_uninit_perl(void) {
	PERL_SYS_TERM();

	perl_destruct(my_perl);
	perl_free(my_perl);
	my_perl = NULL;
}

/******************************************************************************
 * GObject Stuff
 *****************************************************************************/
static void
gplugin_perl_loader_init(G_GNUC_UNUSED GPluginPerlLoader *loader) {
}

static void
gplugin_perl_loader_class_init(GPluginPerlLoaderClass *klass) {
	GPluginLoaderClass *loader_class = GPLUGIN_LOADER_CLASS(klass);

	loader_class->supported_extensions =
		gplugin_perl_loader_class_supported_extensions;
	loader_class->query = gplugin_perl_loader_query;
	loader_class->load = gplugin_perl_loader_load;
	loader_class->unload = gplugin_perl_loader_unload;

	/* perl initialization */
	gplugin_perl_loader_init_perl();
}

static void
gplugin_perl_loader_class_finalize(G_GNUC_UNUSED GPluginPerlLoaderClass *klass)
{
	/* perl uninitialization */
	gplugin_perl_loader_uninit_perl();
}

/******************************************************************************
 * API
 *****************************************************************************/
void
gplugin_perl_loader_register(GPluginNativePlugin *plugin) {
	gplugin_perl_loader_register_type(G_TYPE_MODULE(plugin));
}
